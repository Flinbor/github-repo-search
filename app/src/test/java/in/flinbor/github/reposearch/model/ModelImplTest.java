/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.model;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import in.flinbor.github.reposearch.BaseTest;
import in.flinbor.github.reposearch.constant.Constants;
import in.flinbor.github.reposearch.model.api.ApiInterface;
import in.flinbor.github.reposearch.model.dto.RepositoriesDTO;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.observers.TestSubscriber;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.when;


/**
 * Local test of {@link ModelImpl}
 */
public class ModelImplTest extends BaseTest {
    @Mock
    private ApiInterface apiInterface;
    private Model model;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        model = new ModelImpl(apiInterface, AndroidSchedulers.mainThread(), AndroidSchedulers.mainThread());
    }

    @Test
    public void testGetRepoList() {
        RepositoriesDTO repositoriesDTO = testUtils.readJson("json/repos.json", RepositoriesDTO.class);

        when(apiInterface.getRepositories(Constants.PATH_SEARCH, Constants.PATH_REPOSITORIES, "query", null)).thenReturn(Observable.just(repositoriesDTO));

        TestSubscriber<RepositoriesDTO> testSubscriber = new TestSubscriber<>();
        Observable<RepositoriesDTO> repoList = model.getRepoList("query");

        repoList.subscribe(testSubscriber);

        testSubscriber.assertNoErrors();
        testSubscriber.assertValueCount(1);

        RepositoriesDTO repoDTO = testSubscriber.getOnNextEvents().get(0);

        assertEquals(2, repoDTO.getTotalCount());
        assertEquals(2, repoDTO.getItems().size());
        assertEquals(43213660, repoDTO.getItems().get(0).getId());
        assertEquals("CppCon2015", repoDTO.getItems().get(0).getName());
        assertEquals("CppCon", repoDTO.getItems().get(0).getOwner().getLogin());
    }

}
