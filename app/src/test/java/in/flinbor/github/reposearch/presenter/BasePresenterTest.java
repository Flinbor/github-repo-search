/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.presenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;

import in.flinbor.github.reposearch.BaseTest;
import in.flinbor.github.reposearch.model.Model;
import in.flinbor.github.reposearch.presenter.mapper.RepoMapper;
import rx.Subscription;
import rx.observers.TestSubscriber;

import static junit.framework.Assert.assertTrue;

/**
 * Local test of {@link BasePresenter}
 */
public class BasePresenterTest extends BaseTest {
    private BasePresenter basePresenter;
    @Mock
    private Model model;
    @Mock
    private RepoMapper mapper;

    @Before
    public void setUp() throws Exception {
        super.setUp();

        basePresenter = new RepoListPresenterImpl(mapper);
        basePresenter.setModel(model);
    }


    @Test
    public void testAddSubscription() throws Exception {
        Subscription test = new TestSubscriber<>();
        basePresenter.addSubscription(test);
        assertTrue(basePresenter.compositeSubscription.hasSubscriptions());
    }

    @Test
    public void testOnStop() throws Exception {
        Subscription test = new TestSubscriber<>();
        basePresenter.addSubscription(test);
        basePresenter.onStop();
        assertTrue(test.isUnsubscribed());
    }

    @Test
    public void testOnStopManySubscription() throws Exception {
        final int num_subscription = 100;
        ArrayList<Subscription> subscriptionList = new ArrayList<>();

        for (int i = 0; i < num_subscription; i++) {
            Subscription test = new TestSubscriber<>();
            basePresenter.addSubscription(test);
            subscriptionList.add(test);
        }

        basePresenter.onStop();

        for (Subscription subscription : subscriptionList) {
            assertTrue(subscription.isUnsubscribed());
        }
    }
}