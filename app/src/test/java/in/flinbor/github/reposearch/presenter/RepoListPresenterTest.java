/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.presenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import in.flinbor.github.reposearch.BaseTest;
import in.flinbor.github.reposearch.model.Model;
import in.flinbor.github.reposearch.model.dto.RepositoriesDTO;
import in.flinbor.github.reposearch.presenter.mapper.RepoMapper;
import in.flinbor.github.reposearch.presenter.vo.RepoItem;
import in.flinbor.github.reposearch.view.fragments.RepoListView;
import rx.Observable;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;

/**
 * Local test of {@link RepoListPresenterImpl}
 */
public class RepoListPresenterTest extends BaseTest {

    private RepoListPresenterImpl repoListPresenter;
    @Mock
    private RepoListView mockView;
    @Mock
    private Model model;
    @Mock
    private RepoMapper mapper;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        repoListPresenter = new RepoListPresenterImpl(mapper);
        repoListPresenter.setModel(model);
        repoListPresenter.setView(mockView);
    }

    @Test
    public void testOnItemSelected() {
        RepoItem item = new RepoItem();
        repoListPresenter.onItemSelected(item);
        verify(mockView).openReportDetails(item);
    }

    @Test
    public void testInvalidQuery() {
        repoListPresenter.onSearchQueryChanged("");
        verify(mockView).showEmptyList();
    }

    @Test
    public void testLoadData() {
        RepositoriesDTO inputList = new RepositoriesDTO();

        List<RepoItem> outputRepoList = new ArrayList<>();
        RepoItem repoItem = new RepoItem();
        outputRepoList.add(repoItem);

        doAnswer(invocation -> Observable.just(inputList))
                .when(model)
                .getRepoList("abc");

        doAnswer((invocation -> outputRepoList)).when(mapper).call(inputList);

        repoListPresenter.onSearchQueryChanged("abc");

        verify(mockView).showReports(outputRepoList);
    }


}
