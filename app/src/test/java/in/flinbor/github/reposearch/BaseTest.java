/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import in.flinbor.github.reposearch.model.api.ApiInterfaceTest;

/**
 * Base class for Local Unit Tests
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class,
        sdk = 21)
@Ignore
public class BaseTest {
    protected static TestUtils testUtils;

    /**
     * Common initialization of tests
     *
     * @throws Exception {@link ApiInterfaceTest} and another child can throw throws IOException
     */
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        testUtils = new TestUtils();
    }
}
