/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.view.fragments;

import android.os.Bundle;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.robolectric.Robolectric;

import in.flinbor.github.reposearch.BaseTest;
import in.flinbor.github.reposearch.R;
import in.flinbor.github.reposearch.presenter.RepoListPresenter;
import in.flinbor.github.reposearch.view.MainActivity;

import static org.mockito.Mockito.verify;

/**
 * Local test of {@link RepoListFragment}
 */
public class RepoListFragmentTest extends BaseTest {
    @Mock
    private RepoListPresenter repoListPresenter;
    private RepoListFragment repoListFragment;
    private MainActivity activity;
    private Bundle bundle;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        repoListFragment = new RepoListFragment();
        repoListFragment.setPresenter(repoListPresenter);
        activity = Robolectric.setupActivity(MainActivity.class);
        bundle = Bundle.EMPTY;
    }


    @Test
    public void testOnCreateView() {
        repoListFragment.onViewCreated(activity.findViewById(R.id.activity_main_frame_layout), null);
        verify(repoListPresenter).onViewCreated(null);
    }

    @Test
    public void testOnCreateViewWithBundle() {
        repoListFragment.onViewCreated(activity.findViewById(R.id.activity_main_frame_layout), bundle);
        verify(repoListPresenter).onViewCreated(bundle);
    }

    @Test
    public void testOnStop() {
        repoListFragment.onStop();
        verify(repoListPresenter).onStop();
    }

    @Test
    public void testOnSaveInstanceState() {
        repoListFragment.onSaveInstanceState(null);
        verify(repoListPresenter).onSaveInstanceState(null);
    }

    @Test
    public void testOnSaveInstanceStateWithBundle() {
        repoListFragment.onSaveInstanceState(bundle);
        verify(repoListPresenter).onSaveInstanceState(bundle);
    }


}