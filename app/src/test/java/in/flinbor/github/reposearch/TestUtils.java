/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;

/**
 * Class Utils, contains helper methods local unit tests
 */
public class TestUtils {
    private final Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();

    /**
     * Print log to terminal
     *
     * @param log - text to print
     */
    public void log(String log) {
        System.out.println(log);
    }

    /**
     * Read file with json from test resources, and return it as specified object
     *
     * @param fileName Url of file with json
     * @param inClass Class of T
     * @param <T> Type of the desired object
     * @return Object created from json
     */
    public <T> T readJson(String fileName, Class<T> inClass) {
        return gson.fromJson(readString(fileName), inClass);
    }

    /**
     * Read file with json from test resources, and return it as specified object
     *
     * @param fileName Url of file with json
     * @param inClass Specific generalized type
     * @param <T> Type of the desired object
     * @return Object created from json
     */
    public <T> T readJson(String fileName, Type inClass) {
        return gson.fromJson(readString(fileName), inClass);
    }

    /**
     * Read file with json from test resources, and return it as String
     *
     * @param fileName Url of file with json
     * @return String created from json
     */
    public String readString(String fileName) {
        InputStream stream = getClass().getClassLoader().getResourceAsStream(fileName);
        try {
            int size = stream.available();
            byte[] buffer = new byte[size];
            //noinspection ResultOfMethodCallIgnored
            stream.read(buffer);
            return new String(buffer, "utf8");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
