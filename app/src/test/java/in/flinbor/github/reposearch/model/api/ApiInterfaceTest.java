/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.model.api;

import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.mockwebserver.Dispatcher;
import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;
import com.squareup.okhttp.mockwebserver.RecordedRequest;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import in.flinbor.github.reposearch.BaseTest;
import in.flinbor.github.reposearch.BuildConfig;
import in.flinbor.github.reposearch.constant.Constants;
import in.flinbor.github.reposearch.model.dto.RepositoriesDTO;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Local test of {@link ApiInterface}
 */
public class ApiInterfaceTest extends BaseTest {

    private MockWebServer server;
    private ApiInterface apiInterface;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        server = new MockWebServer();
        server.start();
        final Dispatcher dispatcher = new Dispatcher() {

            @Override
            public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
                testUtils.log("request.getPath() = "+request.getPath());
                if (request.getPath().equals("/" + Constants.PATH_SEARCH + "/" + Constants.PATH_REPOSITORIES + "?q=query")) {
                    return new MockResponse().setResponseCode(200)
                            .setBody(testUtils.readString("json/repos.json"));
                }
                return new MockResponse().setResponseCode(404);
            }
        };

        server.setDispatcher(dispatcher);
        HttpUrl baseUrl = server.url("/");
        testUtils.log("baseUrl = "+baseUrl);
        apiInterface = ApiModule.getApiInterface(baseUrl.toString());
    }


    @Test
    public void testGetRepositories() throws Exception {

        TestSubscriber<RepositoriesDTO> testSubscriber = new TestSubscriber<>();
        apiInterface.getRepositories(Constants.PATH_SEARCH, Constants.PATH_REPOSITORIES, "query", BuildConfig.ACCESS_TOKEN).subscribe(testSubscriber);

        testSubscriber.assertNoErrors();
        testSubscriber.assertValueCount(1);

        final RepositoriesDTO repositoriesDTO = testSubscriber.getOnNextEvents().get(0);

        Assert.assertEquals(2, repositoriesDTO.getTotalCount());
        Assert.assertEquals(2, repositoriesDTO.getItems().size());
        Assert.assertEquals(43213660, repositoriesDTO.getItems().get(0).getId());
        Assert.assertEquals("CppCon2015", repositoriesDTO.getItems().get(0).getName());
        Assert.assertEquals("CppCon", repositoriesDTO.getItems().get(0).getOwner().getLogin());
    }

    @Test
    public void testGetRepositoriesIncorrect() throws Exception {
        try {
            apiInterface.getRepositories(Constants.PATH_SEARCH, "incorect request", null, null).subscribe();
            fail();
        } catch (Exception expected) {
            assertEquals("HTTP 404 OK", expected.getMessage());
        }
    }

    @After
    public void shutDown() throws Exception {
        server.shutdown();
    }
}