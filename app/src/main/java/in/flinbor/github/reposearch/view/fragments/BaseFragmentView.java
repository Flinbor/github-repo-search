/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.view.fragments;

import android.support.annotation.NonNull;

import in.flinbor.github.reposearch.view.ActivityNotifierListener;

/**
 * Base MVP view for fragments
 */
public interface BaseFragmentView {
    /**
     * Set listener to view, Can be used for communication with activity
     *
     * @param listener To communicate with activity
     */
    void setActivityNotifierListener(@NonNull ActivityNotifierListener listener);

    /**
     * Remove listener from view
     *
     * @param listener To communicate with activity
     */
    void removeActivityNotifierListener(@NonNull ActivityNotifierListener listener);
}
