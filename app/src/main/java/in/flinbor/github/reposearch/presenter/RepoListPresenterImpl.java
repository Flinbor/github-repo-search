/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import in.flinbor.github.reposearch.model.dto.RepositoriesDTO;
import in.flinbor.github.reposearch.presenter.mapper.RepoMapper;
import in.flinbor.github.reposearch.presenter.vo.RepoItem;
import in.flinbor.github.reposearch.view.fragments.RepoListView;
import rx.Observable;
import rx.Observer;
import rx.Subscription;


/**
 * Concrete implementation of Presenter
 * responsible for Repository-Screen related business logic
 */
public class RepoListPresenterImpl extends BasePresenter implements RepoListPresenter {
    public static final String TAG = RepoListPresenterImpl.class.getSimpleName();
    public static final String QUERY = "query";
    private final RepoMapper repoMapper;
    private RepoListView view;
    private Subscription subscription;
    private String query;

    public RepoListPresenterImpl(RepoMapper mapper) {
        repoMapper = mapper;
    }

    @Override
    public void onViewCreated(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            String query = savedInstanceState.getString(QUERY);
            if (query != null) {
                onSearchQueryChanged(query);
            }
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(QUERY, query);
    }

    @Override
    public void setView(@NonNull RepoListView view) {
        this.view = view;
    }

    @Override
    public void onItemSelected(RepoItem item) {
        view.openReportDetails(item);
    }

    @Override
    public void onSearchQueryChanged(@NonNull String query) {
        this.query = query;
        if (query.length() > 1) {
            // Load new data when query longer then 1 character
            loadReports(query);
        } else {
            view.showEmptyList();
        }
    }

    private void loadReports(String query) {
        if (subscription != null) {
            // Retrofit RxJava connector will redirect this call to okHttp's cancel
            subscription.unsubscribe();
        }
        Observable<RepositoriesDTO> repoList = getModel().getRepoList(query);
        subscription = repoList
                .map(repoMapper)
                .subscribe(new Observer<List<RepoItem>>() {

                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError");
                        view.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(List<RepoItem> list) {
                        Log.d(TAG, "onNext");
                        if (list == null || list.size() == 0) {
                            Log.d(TAG, "list is null or empty");
                            view.showEmptyList();
                        } else {
                            Log.d(TAG, "list with data received");
                            view.showReports(list);
                        }
                    }
                });

        addSubscription(subscription);
    }
}
