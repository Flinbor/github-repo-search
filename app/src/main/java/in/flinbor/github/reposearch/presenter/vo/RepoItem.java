/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.presenter.vo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Repository View Object
 * Adapted object for UI
 */
public class RepoItem implements Parcelable {
    private int id;
    private String ownerAvatarUrl;
    private String repoName;
    private String description;
    private String subscribersUrl;
    private int forksCount;

    public RepoItem(int id, String ownerAvatarUrl, String repoName, String description, String subscribersUrl, int forksCount) {
        this.id = id;
        this.ownerAvatarUrl = ownerAvatarUrl;
        this.repoName = repoName;
        this.description = description;
        this.subscribersUrl = subscribersUrl;
        this.forksCount = forksCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOwnerAvatarUrl() {
        return ownerAvatarUrl;
    }

    public void setOwnerAvatarUrl(String ownerAvatarUrl) {
        this.ownerAvatarUrl = ownerAvatarUrl;
    }

    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getForksCount() {
        return forksCount;
    }

    public void setForksCount(int forksCount) {
        this.forksCount = forksCount;
    }

    public String getSubscribersUrl() {
        return subscribersUrl;
    }

    public void setSubscribersUrl(String subscribersUrl) {
        this.subscribersUrl = subscribersUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RepoItem repoItem = (RepoItem) o;

        return id == repoItem.id;

    }

    @Override
    public int hashCode() {
        return id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.ownerAvatarUrl);
        dest.writeString(this.repoName);
        dest.writeString(this.description);
        dest.writeString(this.subscribersUrl);
        dest.writeInt(this.forksCount);
    }

    public RepoItem() {
    }

    protected RepoItem(Parcel in) {
        this.id = in.readInt();
        this.ownerAvatarUrl = in.readString();
        this.repoName = in.readString();
        this.description = in.readString();
        this.subscribersUrl = in.readString();
        this.forksCount = in.readInt();
    }

    public static final Parcelable.Creator<RepoItem> CREATOR = new Parcelable.Creator<RepoItem>() {
        @Override
        public RepoItem createFromParcel(Parcel source) {
            return new RepoItem(source);
        }

        @Override
        public RepoItem[] newArray(int size) {
            return new RepoItem[size];
        }
    };
}
