/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.view.fragments;

import android.support.annotation.NonNull;

import java.util.List;

import in.flinbor.github.reposearch.presenter.RepoDetailsPresenter;
import in.flinbor.github.reposearch.presenter.vo.Subscriber;


/**
 * View interface that displays data and routes user commands to the presenter to act upon that data
 * Designed to show Details of Repository
 */
public interface RepoDetailsView extends BaseFragmentView{

    /**
     * Set stub
     */
    void showEmptyList();

    /**
     * Show list of subscribers in view
     *
     * @param subscribersList List to show
     */
    void showSubscribers(@NonNull List<Subscriber> subscribersList);

    /**
     * Show error occurred on subscribers loading
     *
     * @param error Text of error
     */
    void showError(@NonNull String error);

    /**
     * Set presenter interface to View layer
     *
     * @param presenter {@link RepoDetailsPresenter}
     */
    void setPresenter(@NonNull RepoDetailsPresenter presenter);

}
