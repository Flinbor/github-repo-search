/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.view.adapter;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.flinbor.github.reposearch.R;
import in.flinbor.github.reposearch.presenter.RepoDetailsPresenter;
import in.flinbor.github.reposearch.presenter.vo.Subscriber;
import in.flinbor.github.reposearch.util.CropCircleTransformation;


public class SubscribersListAdapter extends BaseAdapter<Subscriber> {

    private Fragment context;
    private RepoDetailsPresenter presenter;

    /**
     * @param fragment used by Glide, (allow Glide manage loading according context lifecycle)
     * @param presenter required for dispatch actions to presenter, for example: "on item selected"
     */
    public SubscribersListAdapter(Fragment fragment, RepoDetailsPresenter presenter) {
        super(Collections.emptyList());
        this.context = fragment;
        this.presenter = presenter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_repo_details_item, viewGroup, false);
        return new SubscribersListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(BaseAdapter.ViewHolder baseHolder, int i) {
        SubscribersListAdapter.ViewHolder holder;
        if (baseHolder instanceof SubscribersListAdapter.ViewHolder) {
            holder = (ViewHolder) baseHolder;
        } else {
            Log.e(this.getClass().getSimpleName(), "method onCreateViewHolder should return instance of local ViewHolder");
            return;
        }
        Subscriber subscriber = list.get(i);
        holder.textViewLogin.setText(subscriber.getLogin());

        String pictureUrl = subscriber.getAvatarUrl();
        if (pictureUrl != null) {
            Glide.with(context)
                    .load(pictureUrl)
                    .centerCrop()
                    .bitmapTransform(new CropCircleTransformation(context.getContext()))
                    .dontAnimate()
                    .placeholder(R.drawable.ic_github_placeholder)
                    .thumbnail(0.2f) //show to user preloaded image with 20% of original, before full sized image will be downloaded
                    .into(holder.imageViewThumbnail);
        } else {
            holder.imageViewThumbnail.setImageResource(R.drawable.ic_github_placeholder);
        }

    }


    class ViewHolder extends BaseAdapter.ViewHolder {
        @BindView(R.id.repo_list_details_text_view_login)  TextView textViewLogin;
        @BindView(R.id.repo_list_details_image_view_thumb) ImageView imageViewThumbnail;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
