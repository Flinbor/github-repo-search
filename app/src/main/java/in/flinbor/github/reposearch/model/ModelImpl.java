/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.model;

import android.support.annotation.NonNull;

import java.util.List;

import in.flinbor.github.reposearch.BuildConfig;
import in.flinbor.github.reposearch.constant.Constants;
import in.flinbor.github.reposearch.model.api.ApiInterface;
import in.flinbor.github.reposearch.model.dto.RepositoriesDTO;
import in.flinbor.github.reposearch.model.dto.SubscriberDTO;
import rx.Observable;
import rx.Scheduler;


/**
 * Implementation of Model
 * responsible for receiving
 */
public class ModelImpl implements Model {

    private final ApiInterface  apiInterface;
    private final Scheduler     subscribeOn;
    private final Scheduler     observeOn;

    /**
     * Constructor with customizable parameters
     *
     * @param apiInterface Api interface or mock
     * @param subscribeOn Thread on which will be performed main calculation (for the JUnit should be mocked with main thread)
     * @param observeOn Thread to with will be dispatched result (for the JUnit should be mocked with main thread)
     */
    public ModelImpl(@NonNull ApiInterface apiInterface, @NonNull Scheduler subscribeOn, @NonNull Scheduler observeOn) {
        this.apiInterface = apiInterface;
        this.subscribeOn = subscribeOn;
        this.observeOn = observeOn;
    }

    @Override
    public Observable<RepositoriesDTO> getRepoList(String query) {
        return apiInterface
                .getRepositories(Constants.PATH_SEARCH, Constants.PATH_REPOSITORIES, query, BuildConfig.ACCESS_TOKEN)
                .compose(applySchedulers());
    }

    @Override
    public Observable<List<SubscriberDTO>> getSubscribersList(String url) {
        return apiInterface
                .getRepositorySubscribers(url).compose(applySchedulers());
    }

    private <T> Observable.Transformer<T, T> applySchedulers() {
        return observable -> observable.subscribeOn(subscribeOn)
                .observeOn(observeOn);
    }

}
