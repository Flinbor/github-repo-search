/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.model.dto;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubscriberDTO {
    @SerializedName("login")
    @Expose
    public String login;
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("avatar_url")
    @Expose
    public String avatarUrl;
    @SerializedName("gravatar_id")
    @Expose
    public String gravatarId;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("html_url")
    @Expose
    public String htmlUrl;
    @SerializedName("followers_url")
    @Expose
    public String followersUrl;
    @SerializedName("following_url")
    @Expose
    public String followingUrl;
    @SerializedName("gists_url")
    @Expose
    public String gistsUrl;
    @SerializedName("starred_url")
    @Expose
    public String starredUrl;
    @SerializedName("subscriptions_url")
    @Expose
    public String subscriptionsUrl;
    @SerializedName("organizations_url")
    @Expose
    public String organizationsUrl;
    @SerializedName("repos_url")
    @Expose
    public String reposUrl;
    @SerializedName("eventsUrl")
    @Expose
    public String eventsUrl;
    @SerializedName("received_events_url")
    @Expose
    public String receivedEventsUrl;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("site_admin")
    @Expose
    public boolean siteAdmin;

    /**
     * @return The login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * @return avatarUrl
     */
    public String getAvatarUrl() {
        return avatarUrl;
    }

    /**
     * @return gravatarId
     */
    public String getGravatarId() {
        return gravatarId;
    }

    /**
     * @return url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @return htmlUrl
     */
    public String getHtmlUrl() {
        return htmlUrl;
    }

    /**
     * @return followersUrl
     */
    public String getFollowersUrl() {
        return followersUrl;
    }

    /**
     * @return followingUrl
     */
    public String getFollowingUrl() {
        return followingUrl;
    }

    /**
     * @return gistsUrl
     */
    public String getGistsUrl() {
        return gistsUrl;
    }

    /**
     * @return starredUrl
     */
    public String getStarredUrl() {
        return starredUrl;
    }

    /**
     * @return subscriptionsUrl
     */
    public String getSubscriptionsUrl() {
        return subscriptionsUrl;
    }

    /**
     * @return organizationsUrl
     */
    public String getOrganizationsUrl() {
        return organizationsUrl;
    }

    /**
     * @return reposUrl
     */
    public String getReposUrl() {
        return reposUrl;
    }

    /**
     * @return eventsUrl
     */
    public String getEvents_url() {
        return eventsUrl;
    }

    /**
     * @return receivedEventsUrl
     */
    public String getReceivedEventsUrl() {
        return receivedEventsUrl;
    }

    /**
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * @return siteAdmin
     */
    public boolean isSiteAdmin() {
        return siteAdmin;
    }
}
