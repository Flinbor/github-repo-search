/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.view.adapter;

import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.Collections;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.flinbor.github.reposearch.R;
import in.flinbor.github.reposearch.presenter.RepoListPresenter;
import in.flinbor.github.reposearch.presenter.vo.RepoItem;


public class RepoListAdapter extends BaseAdapter<RepoItem> {

    private Fragment context;
    private RepoListPresenter presenter;

    /**
     * @param fragment used by Glide, (allow Glide manage loading according context lifecycle)
     * @param presenter required for dispatch actions to presenter, for example: "on item selected"
     */
    public RepoListAdapter(Fragment fragment, RepoListPresenter presenter) {
        super(Collections.emptyList());
        this.context = fragment;
        this.presenter = presenter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_repo_list_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(BaseAdapter.ViewHolder baseHolder, int i) {
        RepoListAdapter.ViewHolder holder;
        if (baseHolder instanceof RepoListAdapter.ViewHolder) {
            holder = (ViewHolder) baseHolder;
        } else {
            Log.e(this.getClass().getSimpleName(), "method onCreateViewHolder should return instance of local ViewHolder");
            return;
        }

        RepoItem repo = list.get(i);
        if (TextUtils.isEmpty(repo.getDescription())) {
            holder.textViewDescription.setVisibility(View.GONE);
        } else {
            holder.textViewDescription.setVisibility(View.VISIBLE);
            holder.textViewDescription.setText(repo.getDescription());
        }

        String name = repo.getRepoName() != null ? repo.getRepoName() : "";
        holder.textViewRepoName.setText(name);

        final String forkPlaceholder = context.getString(R.string.repo_list_placeholder_fork_count);
        final String formatedForkCount = String.format(Locale.getDefault(), forkPlaceholder, repo.getForksCount());
        holder.textViewForksCount.setText(formatedForkCount);

        String pictureUrl = repo.getOwnerAvatarUrl();
        if (pictureUrl != null) {
            Glide.with(context)
                    .load(pictureUrl)
                    .centerCrop()
                    .dontAnimate()
                    .placeholder(R.drawable.ic_github_placeholder)
                    .thumbnail(0.2f) //show to user preloaded image with 20% of original, before full sized image will be downloaded
                    .into(holder.imageViewThumbnail);
        } else {
            holder.imageViewThumbnail.setImageResource(R.drawable.ic_github_placeholder);
        }

        holder.cardView.setOnClickListener(v ->
                presenter.onItemSelected(repo));
    }

    class ViewHolder extends BaseAdapter.ViewHolder {
        @BindView(R.id.repo_list_item_card_view)               View cardView;
        @BindView(R.id.repo_list_item_text_view_description)   TextView textViewDescription;
        @BindView(R.id.repo_list_item_text_view_forks_count)   TextView textViewForksCount;
        @BindView(R.id.repo_list_item_text_view_repo_name)     TextView textViewRepoName;
        @BindView(R.id.repo_list_item_image_view_thumbnail)
        ImageView imageViewThumbnail;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
