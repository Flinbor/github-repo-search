/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.model.api;


import java.util.List;

import in.flinbor.github.reposearch.model.dto.RepositoriesDTO;
import in.flinbor.github.reposearch.model.dto.SubscriberDTO;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;
import rx.Observable;


/**
 * Retrofit interface for GitHub API
 */
public interface ApiInterface {

    @GET("{search}/{repositories}")
    Observable<RepositoriesDTO> getRepositories(@Path("search") String search,
                                                @Path("repositories") String repositories,
                                                @Query("q") String query,
                                                @Query("access_token") String access_token);

    @GET
    Observable<List<SubscriberDTO>> getRepositorySubscribers(@Url String url);

}
