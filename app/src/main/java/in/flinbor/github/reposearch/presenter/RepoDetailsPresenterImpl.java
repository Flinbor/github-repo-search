/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import java.util.List;

import in.flinbor.github.reposearch.R;
import in.flinbor.github.reposearch.app.App;
import in.flinbor.github.reposearch.model.dto.SubscriberDTO;
import in.flinbor.github.reposearch.presenter.mapper.SubscribersMapper;
import in.flinbor.github.reposearch.presenter.vo.RepoItem;
import in.flinbor.github.reposearch.presenter.vo.Subscriber;
import in.flinbor.github.reposearch.view.fragments.RepoDetailsView;
import rx.Observable;
import rx.Observer;
import rx.Subscription;


/**
 * Concrete implementation of Presenter
 * responsible for Repository-Screen related business logic
 */
public class RepoDetailsPresenterImpl extends BasePresenter implements RepoDetailsPresenter {
    public static final String TAG = RepoDetailsPresenterImpl.class.getSimpleName();
    private final SubscribersMapper subscribersMapper;
    private RepoDetailsView view;
    private Subscription subscription;

    public RepoDetailsPresenterImpl() {
        subscribersMapper = new SubscribersMapper();
    }

    @Override
    public void onViewCreated(Bundle savedInstanceState) {

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        //todo: save view state
    }

    @Override
    public void setView(@NonNull RepoDetailsView view) {
        this.view = view;
    }

    @Override
    public void setRepository(@NonNull RepoItem repository) {
        final String subscribersUrl = repository.getSubscribersUrl();
        if (TextUtils.isEmpty(subscribersUrl)) {
            view.showError(App.getApp().getString(R.string.repo_details_info_load_emptylist));
        } else {
            loadSubscribers(repository);
        }
    }


    private void loadSubscribers(RepoItem repository) {

        Observable<List<SubscriberDTO>> repoList = getModel().getSubscribersList(repository.getSubscribersUrl());
        subscription = repoList
                .map(subscribersMapper)
                .subscribe(new Observer<List<Subscriber>>() {

                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError");
                        view.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(List<Subscriber> list) {
                        Log.d(TAG, "onNext");
                        if (list == null || list.size() == 0) {
                            Log.d(TAG, "list is null or empty");
                            view.showEmptyList();
                        } else {
                            Log.d(TAG, "list with data received");
                            view.showSubscribers(list);
                        }
                    }
                });

        addSubscription(subscription);
    }
}
