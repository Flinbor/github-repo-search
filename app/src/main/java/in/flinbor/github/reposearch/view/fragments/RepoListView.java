/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.view.fragments;

import android.support.annotation.NonNull;

import java.util.List;

import in.flinbor.github.reposearch.presenter.RepoListPresenter;
import in.flinbor.github.reposearch.presenter.vo.RepoItem;


/**
 * View interface that displays data and routes user commands to the presenter to act upon that data
 * Designed to show Repositories
 */
public interface RepoListView extends BaseFragmentView{

    /**
     * Set stub
     */
    void showEmptyList();

    /**
     * Show list of repository in view
     *
     * @param repoList List to show
     */
    void showReports(@NonNull List<RepoItem> repoList);

    /**
     * Show error occurred on repository loading
     *
     * @param error Text to show
     */
    void showError(@NonNull String error);

    /**
     * Set presenter interface to View layer
     *
     * @param presenter {@link RepoListPresenter}
     */
    void setPresenter(@NonNull RepoListPresenter presenter);

    /**
     * Open details of GitHub Report
     *
     * @param repoItem {@link RepoItem} that should be opened
     */
    void openReportDetails(@NonNull RepoItem repoItem);

}
