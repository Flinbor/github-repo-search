/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.presenter;


import in.flinbor.github.reposearch.model.Model;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Base implementation of Presenter
 * contains common functionality for presenters
 * registration of rx subscription
 */
public abstract class BasePresenter implements Presenter {
    protected final CompositeSubscription compositeSubscription = new CompositeSubscription();
    private Model model;

    /**
     * Subscribe on RX events
     * @param subscription {@link Subscription} that should be added to this {@code CompositeSubscription}
     */
    protected void addSubscription(Subscription subscription) {
        compositeSubscription.add(subscription);
    }

    /**
     * Stop RX events
     */
    protected void removeSubscription() {
        compositeSubscription.unsubscribe();
    }

    /**
     * Unsubscribe all subscriptions that are currently part of this {@code CompositeSubscription}
     */
    @Override
    public void onStop() {
        compositeSubscription.clear();
    }

    @Override
    public Model getModel() {
        return model;
    }

    @Override
    public void setModel(Model model) {
        this.model = model;
    }
}
