/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.presenter;

import android.support.annotation.NonNull;

import in.flinbor.github.reposearch.presenter.vo.RepoItem;
import in.flinbor.github.reposearch.view.fragments.RepoListView;


/**
 * Extension of presenter interface
 * Added methods that specified for {@link RepoListView}
 */
public interface RepoListPresenter extends Presenter {

    /**
     * Set View interface to Presenter layer
     *
     * @param view {@link RepoListView}
     */
    void setView(@NonNull RepoListView view);

    /**
     * triggered from view when item selected by user
     *
     * @param repo The Repository that was selected
     */
    void onItemSelected(RepoItem repo);

    /**
     * Search reports query changed
     *
     * @param query The text typed by user
     */
    void onSearchQueryChanged(@NonNull String query);
}
