/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.view.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.flinbor.github.reposearch.R;
import in.flinbor.github.reposearch.presenter.RepoDetailsPresenter;
import in.flinbor.github.reposearch.presenter.vo.RepoItem;
import in.flinbor.github.reposearch.presenter.vo.Subscriber;
import in.flinbor.github.reposearch.view.ActivityNotifierListener;
import in.flinbor.github.reposearch.view.adapter.SubscribersListAdapter;


/**
 * Implementation of view layer, responsible for UI
 * Show list of Subscribers
 */
public class RepoDetailsFragment extends Fragment implements RepoDetailsView {
    public static final String REPO_ITEM = "repo_item";
    @BindView(R.id.recycler_view)
    protected RecyclerView      recyclerView;
    private SubscribersListAdapter adapter;
    private RepoDetailsPresenter presenter;
    private ActivityNotifierListener activityListener;
    private RepoItem repoItem;

    public static Fragment getInstance(RepoItem repoItem) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(REPO_ITEM, repoItem);
        RepoDetailsFragment fragment = new RepoDetailsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void setPresenter(@NonNull RepoDetailsPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setActivityNotifierListener(@NonNull ActivityNotifierListener listener) {
        this.activityListener = listener;
    }

    @Override
    public void removeActivityNotifierListener(@NonNull ActivityNotifierListener listener) {
        activityListener = null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        repoItem = getArguments().getParcelable(REPO_ITEM);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_repo_list, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Initialization of recycler view
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);
        adapter = new SubscribersListAdapter(this, presenter);
        recyclerView.setAdapter(adapter);
        // Notify presenter
        presenter.onViewCreated(savedInstanceState);
        presenter.setRepository(repoItem);
        // Set Fragment title
        activityListener.setActionBarTitle(getString(R.string.repo_details_title) + repoItem.getRepoName());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void showError(@NonNull String error) {
        makeToast(error);
    }


    @Override
    public void showEmptyList() {
        adapter.clear();
    }

    @Override
    public void showSubscribers(@NonNull List<Subscriber> subscribersList) {
        adapter.setList(subscribersList);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (presenter != null) {
            presenter.onSaveInstanceState(outState);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (presenter != null) {
            presenter.onStop();
        }
    }

    /**
     * show short message to user
     * @param text message to show
     */
    private void makeToast(String text) {
        Snackbar.make(recyclerView, text, Snackbar.LENGTH_LONG).show();
    }
}
