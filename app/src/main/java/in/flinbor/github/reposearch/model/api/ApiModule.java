/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.model.api;


import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Class responsible for creation retrofit API client
 */
public class ApiModule {
    private static final boolean ENABLE_LOG = true;
    private static String baseUrl;

    private ApiModule() {
    }

    /**
     * Create retrofit API client with defined URl
     *
     * @param baseUrl Url for API client
     * @return Retrofit API client
     */
    public static ApiInterface getApiInterface(String baseUrl) {
        ApiModule.baseUrl = baseUrl;

        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();

        if (ENABLE_LOG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY); //Logs request headers and bodies
            okHttpBuilder.addInterceptor(interceptor);
        }

        OkHttpClient httpClient = okHttpBuilder.build();

        //build Retrofit based on OKHttp3
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create());

        builder.client(httpClient);

        return builder.build().create(ApiInterface.class);
    }

}
