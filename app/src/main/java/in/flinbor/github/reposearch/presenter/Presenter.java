/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.presenter;

import android.os.Bundle;
import android.support.annotation.Nullable;

import in.flinbor.github.reposearch.model.Model;


/**
 * The presenter acts upon the model and the view.
 * It retrieves data from Model, and formats it for display in the View.
 * designed for work with View that based on {@link android.support.v4.app.Fragment}
 */
public interface Presenter {
    /**
     * Called when view created
     *
     * @param savedInstanceState Bundle to restore state
     */
    void onViewCreated(@Nullable Bundle savedInstanceState);

    /**
     * Called before view will be destroyed
     *
     * @param outState Bundle to store important data to
     */
    void onSaveInstanceState(@Nullable Bundle outState);

    /**
     * Called when view stopped
     */
    void onStop();

    /**
     * set model interface to Presenter layer
     * @param model {@link Model}
     */
    void setModel(Model model);

    /**
     * @return {@link Model} implementation
     */
    Model getModel();
}
