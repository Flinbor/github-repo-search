/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.flinbor.github.reposearch.R;
import in.flinbor.github.reposearch.app.App;
import in.flinbor.github.reposearch.model.Model;
import in.flinbor.github.reposearch.presenter.RepoDetailsPresenter;
import in.flinbor.github.reposearch.presenter.RepoDetailsPresenterImpl;
import in.flinbor.github.reposearch.presenter.RepoListPresenter;
import in.flinbor.github.reposearch.presenter.RepoListPresenterImpl;
import in.flinbor.github.reposearch.presenter.mapper.RepoMapper;
import in.flinbor.github.reposearch.presenter.vo.RepoItem;
import in.flinbor.github.reposearch.view.fragments.BaseFragmentView;
import in.flinbor.github.reposearch.view.fragments.RepoDetailsFragment;
import in.flinbor.github.reposearch.view.fragments.RepoDetailsView;
import in.flinbor.github.reposearch.view.fragments.RepoListFragment;
import in.flinbor.github.reposearch.view.fragments.RepoListView;


/**
 * The activity that is loaded when the application starts
 * controls fragments creation
 */
public class MainActivity extends AppCompatActivity implements ActivityNotifierListener {
    private FragmentManager fragmentManager;

    @BindView(R.id.activity_maion_toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        fragmentManager = getSupportFragmentManager();
        if (savedInstanceState == null) {
            Fragment fragment = RepoListFragment.getInstance();
            replaceFragment(fragment, false);
            injectFragments(fragment);
            if (fragment instanceof BaseFragmentView) {
                ((BaseFragmentView) fragment).setActivityNotifierListener(this);
            }
        } else {
            //add listener ActivityNotifierListener to fragments
            if (fragmentManager.getFragments() != null) {
                for (Fragment fragment : fragmentManager.getFragments()) {
                    if (fragment instanceof BaseFragmentView) {
                        BaseFragmentView view = (BaseFragmentView) fragment;
                        view.setActivityNotifierListener(this);
                        injectFragments(fragment);
                    }
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //remove ActivityNotifierListener from fragments
        if (fragmentManager.getFragments() != null) {
            for (Fragment fragment : fragmentManager.getFragments()) {
                if (fragment instanceof BaseFragmentView) {
                    BaseFragmentView view = (BaseFragmentView) fragment;
                    view.removeActivityNotifierListener(this);
                }
            }
        }
    }

    //todo: replace with Dagger injection
    private void injectFragments(Fragment fragment) {
        if (fragment instanceof RepoListView) {
            //inject mapper for junit test
            RepoMapper mapper = new RepoMapper();
            bindRepoListMVP((RepoListView) fragment, new RepoListPresenterImpl(mapper), App.getApp().getModelInstance());
        } else if (fragment instanceof RepoDetailsView) {
            bindRepoDetailsMVP((RepoDetailsView) fragment, new RepoDetailsPresenterImpl(), App.getApp().getModelInstance());
        }
    }

    /**
     * Creating links between model-view-presenter
     */
    private void bindRepoListMVP(@NonNull RepoListView view, @NonNull RepoListPresenter presenter, @NonNull Model model) {
        view.setPresenter(presenter);
        presenter.setView(view);
        presenter.setModel(model);
    }

    /**
     * Creating links between model-view-presenter
     */
    private void bindRepoDetailsMVP(@NonNull RepoDetailsView view, @NonNull RepoDetailsPresenter presenter, @NonNull Model model) {
        view.setPresenter(presenter);
        presenter.setView(view);
        presenter.setModel(model);
    }

    /**
     * Set fragment to default activity container
     *
     * @param fragment     Fragment to insert
     * @param addBackStack If true -> fragment will be added to backStack
     */
    private void replaceFragment(final Fragment fragment, boolean addBackStack) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.activity_main_frame_layout, fragment, fragment.getClass().getSimpleName());
        if (addBackStack) transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void openRepositoryDetailsFragment(@NonNull RepoItem repoItem) {
        Fragment fragment = RepoDetailsFragment.getInstance(repoItem);
        injectFragments(fragment);
        if (fragment instanceof BaseFragmentView) {
            ((BaseFragmentView) fragment).setActivityNotifierListener(this);
        }
        replaceFragment(fragment, true);
    }

    @Override
    public void setActionBarTitle(@NonNull String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }
}
