/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.model;

import java.util.List;

import in.flinbor.github.reposearch.model.dto.RepositoriesDTO;
import in.flinbor.github.reposearch.model.dto.SubscriberDTO;
import rx.Observable;


/**
 * The model interface defining the data
 */
public interface Model {

    /**
     * Get list of GitHub Repositories
     *
     * @param query The text query that should be in name of repository
     * @return Output as Observable
     */
    Observable<RepositoriesDTO> getRepoList(String query);

    /**
     * Get list of Subscribers to repository
     *
     * @param url To connect
     * @return List of Subscribers
     */
    Observable<List<SubscriberDTO>> getSubscribersList(String url);

}
