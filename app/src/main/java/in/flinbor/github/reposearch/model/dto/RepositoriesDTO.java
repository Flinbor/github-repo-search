/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Data Transfer Object for GitHub Repositories
 */
public class RepositoriesDTO {
    @SerializedName("total_count")
    @Expose
    public int totalCount;
    @SerializedName("incomplete_results")
    @Expose
    public boolean incompleteResults;
    @SerializedName("items")
    @Expose
    public List<RepositoryDTO> items;

    /**
     * @return The total_count
     */
    public int getTotalCount() {
        return totalCount;
    }

    /**
     * @return The incomplete_results
     */
    public boolean isIncompleteResults() {
        return incompleteResults;
    }

    /**
     * @return The items
     */
    public List<RepositoryDTO> getItems() {
        return items;
    }
}
