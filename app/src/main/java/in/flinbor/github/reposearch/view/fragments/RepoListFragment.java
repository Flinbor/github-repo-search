/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.view.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.flinbor.github.reposearch.R;
import in.flinbor.github.reposearch.presenter.RepoListPresenter;
import in.flinbor.github.reposearch.presenter.vo.RepoItem;
import in.flinbor.github.reposearch.view.ActivityNotifierListener;
import in.flinbor.github.reposearch.view.adapter.RepoListAdapter;


/**
 * Implementation of view layer, responsible for UI
 */
public class RepoListFragment extends Fragment implements RepoListView {
    @BindView(R.id.recycler_view)
    protected RecyclerView      recyclerView;
    private RepoListAdapter adapter;
    private RepoListPresenter presenter;
    private ActivityNotifierListener activityListener;

    public static Fragment getInstance() {
        return new RepoListFragment();
    }

    @Override
    public void setPresenter(@NonNull RepoListPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void openReportDetails(@NonNull RepoItem repoItem) {
        activityListener.openRepositoryDetailsFragment(repoItem);
    }

    @Override
    public void setActivityNotifierListener(@NonNull ActivityNotifierListener listener) {
        this.activityListener = listener;
    }

    @Override
    public void removeActivityNotifierListener(@NonNull ActivityNotifierListener listener) {
        activityListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_repo_list, container, false);
        ButterKnife.bind(this, view);
        // Set Fragment title
        activityListener.setActionBarTitle(getString(R.string.repo_list_title));
        initRecyclerVeiw();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Notify presenter
        presenter.onViewCreated(savedInstanceState);
    }

    private void initRecyclerVeiw() {
        // Initialization of recycler view
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);
        adapter = new RepoListAdapter(this, presenter);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.repo_list_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem searchItem = menu.findItem(R.id.menu_report_list_action_search);
        MenuItemCompat.expandActionView(searchItem);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setQueryHint(getString(R.string.menu_report_list_search_hint));

        //set on Query change listeners
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override public boolean onQueryTextSubmit(String query) {return false;}

            @Override
            public boolean onQueryTextChange(String newText) {
                presenter.onSearchQueryChanged(newText);
                return false;
            }
        });

//        searchView.setOnCloseListener(() -> {
//            presenter.onSearchQueryChanged("");
//            return true;
//        });
    }

    @Override
    public void showError(@NonNull String error) {
        makeToast(error);
    }


    @Override
    public void showEmptyList() {
        adapter.clear();
    }

    @Override
    public void showReports(@NonNull List<RepoItem> repoList) {
        adapter.setList(repoList);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (presenter != null) {
            presenter.onSaveInstanceState(outState);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (presenter != null) {
            presenter.onStop();
        }
    }

    /**
     * show short message to user
     * @param text message to show
     */
    private void makeToast(String text) {
        Snackbar.make(recyclerView, text, Snackbar.LENGTH_LONG).show();
    }

}
