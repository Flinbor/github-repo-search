/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.app;


import in.flinbor.github.reposearch.constant.Constants;
import in.flinbor.github.reposearch.model.Model;
import in.flinbor.github.reposearch.model.ModelImpl;
import in.flinbor.github.reposearch.model.api.ApiModule;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Class extend the Android Application class and deal with global variables
 */
public class App extends android.app.Application {
    private static App    app;
    private Model model;

    @Override
    public void onCreate() {
        super.onCreate();
        this.app = this;
        this.model = new ModelImpl(ApiModule.getApiInterface(Constants.BASE_URL), Schedulers.io(), AndroidSchedulers.mainThread());
    }

    /**
     * @return Static instance of {@link App}
     */
    public static App getApp() {
        return app;
    }

    /**
     * @return Instance of {@link Model}
     */
    public Model getModelInstance() {
        return model;
    }

}
