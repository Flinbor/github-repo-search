/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.github.reposearch.presenter.mapper;

import java.util.List;

import in.flinbor.github.reposearch.model.dto.SubscriberDTO;
import in.flinbor.github.reposearch.presenter.vo.Subscriber;
import rx.Observable;
import rx.functions.Func1;


/**
 * Class converter of Data Transfer Object to View Object
 * SubscriberDTO to Subscriber
 */
public class SubscribersMapper implements Func1<List<SubscriberDTO>, List<Subscriber>> {

    public List<Subscriber> call(List<SubscriberDTO> subscriberDTOList) {
        if (subscriberDTOList == null) {
            return null;
        }
        return Observable.from(subscriberDTOList)
                .map(subscriber -> new Subscriber(subscriber.getId(), subscriber.getLogin(), subscriber.getAvatarUrl()))
                .toList()
                .toBlocking()
                .first();
    }

}
