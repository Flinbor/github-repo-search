// Generated code from Butter Knife. Do not modify!
package in.flinbor.github.reposearch.view.adapter;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import java.lang.IllegalStateException;
import java.lang.Object;
import java.lang.Override;

public class RepoListAdapter$ViewHolder$$ViewBinder<T extends RepoListAdapter.ViewHolder> implements ViewBinder<T> {
  @Override
  public Unbinder bind(final Finder finder, final T target, Object source) {
    InnerUnbinder unbinder = createUnbinder(target);
    View view;
    view = finder.findRequiredView(source, 2131492983, "field 'cardView'");
    target.cardView = view;
    view = finder.findRequiredView(source, 2131492985, "field 'textViewDescription'");
    target.textViewDescription = finder.castView(view, 2131492985, "field 'textViewDescription'");
    view = finder.findRequiredView(source, 2131492987, "field 'textViewForksCount'");
    target.textViewForksCount = finder.castView(view, 2131492987, "field 'textViewForksCount'");
    view = finder.findRequiredView(source, 2131492986, "field 'textViewRepoName'");
    target.textViewRepoName = finder.castView(view, 2131492986, "field 'textViewRepoName'");
    view = finder.findRequiredView(source, 2131492984, "field 'imageViewThumbnail'");
    target.imageViewThumbnail = finder.castView(view, 2131492984, "field 'imageViewThumbnail'");
    return unbinder;
  }

  protected InnerUnbinder<T> createUnbinder(T target) {
    return new InnerUnbinder(target);
  }

  protected static class InnerUnbinder<T extends RepoListAdapter.ViewHolder> implements Unbinder {
    private T target;

    protected InnerUnbinder(T target) {
      this.target = target;
    }

    @Override
    public final void unbind() {
      if (target == null) throw new IllegalStateException("Bindings already cleared.");
      unbind(target);
      target = null;
    }

    protected void unbind(T target) {
      target.cardView = null;
      target.textViewDescription = null;
      target.textViewForksCount = null;
      target.textViewRepoName = null;
      target.imageViewThumbnail = null;
    }
  }
}
