// Generated code from Butter Knife. Do not modify!
package in.flinbor.github.reposearch.view.adapter;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import java.lang.IllegalStateException;
import java.lang.Object;
import java.lang.Override;

public class SubscribersListAdapter$ViewHolder$$ViewBinder<T extends SubscribersListAdapter.ViewHolder> implements ViewBinder<T> {
  @Override
  public Unbinder bind(final Finder finder, final T target, Object source) {
    InnerUnbinder unbinder = createUnbinder(target);
    View view;
    view = finder.findRequiredView(source, 2131492982, "field 'textViewLogin'");
    target.textViewLogin = finder.castView(view, 2131492982, "field 'textViewLogin'");
    view = finder.findRequiredView(source, 2131492981, "field 'imageViewThumbnail'");
    target.imageViewThumbnail = finder.castView(view, 2131492981, "field 'imageViewThumbnail'");
    return unbinder;
  }

  protected InnerUnbinder<T> createUnbinder(T target) {
    return new InnerUnbinder(target);
  }

  protected static class InnerUnbinder<T extends SubscribersListAdapter.ViewHolder> implements Unbinder {
    private T target;

    protected InnerUnbinder(T target) {
      this.target = target;
    }

    @Override
    public final void unbind() {
      if (target == null) throw new IllegalStateException("Bindings already cleared.");
      unbind(target);
      target = null;
    }

    protected void unbind(T target) {
      target.textViewLogin = null;
      target.imageViewThumbnail = null;
    }
  }
}
